Event Manager
=============

Event Manager is a school website project built in PHP, that manages your own personal group of people and events associated with them.

Accessible on heroku: [http://event-manager-si.herokuapp.com](http://event-manager-si.herokuapp.com) (Login as admin/admin)

View on Github pages: [http://jakobboss.github.io/event-manager](http://jakobboss.github.io/event-manager)
